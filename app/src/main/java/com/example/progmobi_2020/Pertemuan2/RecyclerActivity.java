package com.example.progmobi_2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmobi_2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmobi_2020.Model.Mahasiswa;
import com.example.progmobi_2020.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvGetMhsAll);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Irene", "72180001", "0812345671");
        Mahasiswa m2 = new Mahasiswa("Seulgi", "72180002", "0812345672");
        Mahasiswa m3 = new Mahasiswa("Wendy", "72180003", "0812345673");
        Mahasiswa m4 = new Mahasiswa("Joy", "72180004", "0812345674");
        Mahasiswa m5 = new Mahasiswa("Yeri", "72180005", "0812345675");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);




    }
}