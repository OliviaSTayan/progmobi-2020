package com.example.progmobi_2020.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmobi_2020.Model.DefaultResult;
import com.example.progmobi_2020.Network.GetDataService;
import com.example.progmobi_2020.Network.RetrofitClientInstance;
import com.example.progmobi_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapusMhsActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_mhs);

        //variabel
        EditText edHapusMhs = (EditText)findViewById(R.id.editTextHapusMhs);
        Button btnHapusMhs = (Button)findViewById(R.id.buttonHapusMhs);
        pd = new ProgressDialog(HapusMhsActivity.this);

        btnHapusMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edHapusMhs.getText().toString(),"72180258"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMhsActivity.this,"DATA BERHASIL DIHAPUS",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMhsActivity.this,"DATA TIDAK BERHASIL DIHAPUS",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}