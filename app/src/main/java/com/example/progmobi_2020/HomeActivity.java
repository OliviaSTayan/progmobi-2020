package com.example.progmobi_2020;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmobi_2020.Crud.MainMhsActivity;
import com.example.progmobi_2020.Crud_Dosen.MainDsnActivity;
import com.example.progmobi_2020.Crud_Matkul.MainMatkulActivity;
import com.example.progmobi_2020.Model.User;
import com.example.progmobi_2020.Pertemuan6.LoginActivity;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //variabel
        ImageView imgMhs = (ImageView)findViewById(R.id.imgMhs);
        ImageView imgDsn = (ImageView)findViewById(R.id.imgDosen);
        ImageView imgMk = (ImageView)findViewById(R.id.imgMatkul);
        ImageView imgLogout = (ImageView)findViewById(R.id.imgLogout);

        //action
        imgMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        imgDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });

        imgMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });

        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = session.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent Intent = new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(Intent);
            }
        });
    }
}